# Wardrobify

Team:

* Trewin - Hats
* Angelo - Shoes

Microservice Two-Shot

Steps to building out our microservices: 
Angelo(shoes) Trewin(hats)
    - Read the project outline, discuss with pair how to approach backend and frontend, find agreement
    -Target the API backend first:
            -Setup models
            -Setup views
        - Test response and request for each element and method (Get, Post, Delete)
        - After testing is complete, both merge all backend work to main and pull changes to development branches

    - Develop the frontend
        - Coordinate with pair how we will access files for testing as we develop
        - We decided to implements routes at the end and use Main page for development testing
        - Decide which types of components will achieve MVP (chose class-based)
        - Develop components, testing after each point of functionality is achieved
        - After components developed, both merge
        - Add routes
    - Test functionality on all components/pages
    - Make final commit
    - Submit project

