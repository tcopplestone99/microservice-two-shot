import React from "react"

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: "",
            style: "",
            color: "",
            picture: "",
            location: "",
            locations: []
        }
        this.handleFabricChange = this.handleFabricChange.bind(this)
        this.handleStyleChange = this.handleStyleChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handlePictureChange = this.handlePictureChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    handleFabricChange(event) {
        const value = event.target.value
        this.setState({fabric: value})
    }

    handleStyleChange(event) {
        const value = event.target.value
        this.setState({style: value})
    }

    handleColorChange(event) {
        const value = event.target.value
        this.setState({color: value})
    }

    handlePictureChange(event) {
        const value = event.target.value
        this.setState({picture: value})
    }

    handleLocationChange(event) {
        const value = event.target.value
        this.setState({location: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.locations
        console.log(data)

        const hatUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headersL: {
                "Content-Type": "application/json"
            }
        }
        const r = await fetch(hatUrl, fetchConfig)
        if(r.ok) {
            const newHat = await r.json()
            console.log(newHat)
            const wipe = {
                fabric: "",
                style: "",
                color: "",
                picture: "",
                location: "",
            }
            this.setState({wipe})
        }

    }

    async componentDidMount() {
        const locUrl = "http://localhost:8100/api/locations/"
        const r = await fetch(locUrl)
        if (r.ok) {
            const data = await r.json()
            this.setState({locations: data.locations})
        }
    }
    render() {
        return (
            <div>
                <h1>Create a new hat</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <input onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric"/>
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div>
                        <input onChange={this.handleStyleChange} placeholder="Style" required type="text" name="style" id="style"/>
                        <label htmlFor="style">Style</label>
                    </div>
                    <div>
                        <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div>
                        <input onChange={this.handlePictureChange} placeholder="Picture" required type="text" name="picture" id="picture"/>
                        <label htmlFor="picture">Picture</label>
                    </div>
                    <div>
                        <select onChange={this.handleLocationChange} required id="location" name="location" value={this.state.location}>
                            <option value="">Choose a location</option>
                            {this.state.locations.map(loc => {
                                return (
                                    <option key={loc.id} value={loc.href}>{loc.closet_name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button>Create</button>
                </form>
            </div>
        )
    }
}


export default HatForm
