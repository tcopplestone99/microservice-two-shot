import React from "react"

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            pictureUrl:"",
            modelName: "",
            manufacturer: "",
            color: "",
            bin: "",
            bins: []
        }
        this.handlePictureChange = this.handlePictureChange.bind(this)
        this.handleModelChange = this.handleModelChange.bind(this)
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleBinChange = this.handleBinChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    handlePictureChange(event) {
        const value = event.target.value
        this.setState({pictureUrl: value})
    }

    handleModelChange(event) {
        const value = event.target.value
        this.setState({modelName: value})
    }

    handleManufacturerChange(event) {
        const value = event.target.value
        this.setState({manufacturer: value})
    }

    handleColorChange(event) {
        const value = event.target.value
        this.setState({color: value})
    }

    handleBinChange(event) {
        const value = event.target.value
        this.setState({bin: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        data.model_name = data.modelName
        data.picture_url = data.pictureUrl
        delete data.bins
        delete data.modelName
        delete data.pictureUrl

        console.log(data)

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headersL: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if(response.ok) {
            const newShoe = await response.json()
            console.log(newShoe)
            const cleared = {
                pictureUrl:"",
                modelName: "",
                manufacturer: "",
                color: "",
                bin: "",
            }
            this.setState(cleared)
        }

    }

    async componentDidMount() {
        const binUrl = "http://localhost:8100/api/bins/"
        const r = await fetch(binUrl)
        if (r.ok) {
            const data = await r.json()
            this.setState({bins: data.bins})
        }
    }
    render() {
        return (
            <div>
                <h1>Create a new shoe</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <input onChange={this.handlePictureChange} placeholder="Picture" required type="text" name="picture_url" id="picture_url"/>
                        <label htmlFor="picture_url">Picture</label>
                    </div>
                    <div>
                        <input onChange={this.handleModelChange} placeholder="Model Name" required type="text" name="model_name" id="model_name"/>
                        <label htmlFor="model_name">Model</label>
                    </div>
                    <div>
                        <input onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer"/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div>
                        <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div>
                        <select onChange={this.handleBinChange} required id="bin" name="bin" value={this.state.bin}>
                            <option value="">Choose a bin</option>
                            {this.state.bins.map(bin => {
                                return (
                                    <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button>Create</button>
                </form>
            </div>
        )
    }
}


export default ShoeForm;
