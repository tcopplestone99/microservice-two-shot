import React from 'react'

class ShoesList extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            shoes: []

        }

    }

    async componentDidMount(){
        const url = ('http://localhost:8080/api/shoes/');
        const response = await fetch(url);

        if (response.ok){
            const data =  await response.json();
            this.setState({shoes: data.shoes});
            console.log(data.shoes);
        }
    }

    async deleteShoe(shoe) {
        const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            const remShoes = this.state.shoes.filter((i) => shoe.id != i.id)
            this.setState({shoes: remShoes})
        }
    }

    render() {
        return (
            <div className='container'>
                <h1>Shoes List</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Picture</th>
                            <th>Model Name</th>
                            <th>Manufacturer</th>
                            <th>Color</th>
                            <th>Closet Name</th>
                            <th>Bin Number</th>
                            <th>Bin Size</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.shoes.map(shoe => {
                            return(
                                <tr key={shoe.id}>
                                    <td><img style={{ width:50 }} src={shoe.picture_url}></img></td>
                                    <td>{shoe.model_name}</td>
                                    <td>{shoe.manufacturer}</td>
                                    <td>{shoe.color}</td>
                                    <td>{shoe.bin.closet_name}</td>
                                    <td>{shoe.bin.bin_number}</td>
                                    <td>{shoe.bin.bin_size}</td>
                                    <td>
                                        <button type='button' className="btn btn-danger" onClick={() => this.deleteShoe(shoe)}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }

}
export default ShoesList;
