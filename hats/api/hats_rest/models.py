from django.db import models

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=500, unique=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=500)
    style = models.CharField(max_length=500)
    color = models.CharField(max_length=50)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )


    def __str__(self):
        return self.fabric
